package com.qualcomm.treinamento_android;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.qualcomm.treinamento_android.network.ContactsHttpManager;
import com.qualcomm.treinamento_android.provider.ContactModel;
import com.qualcomm.treinamento_android.service.ContactSyncService;

import java.io.IOException;

public class AddContactActivity extends Activity implements View.OnClickListener {


    EditText nameEditTextView;
    EditText phoneEditTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);


        findViewById(R.id.btn_insert_contact).setOnClickListener(this);
        nameEditTextView = (EditText) findViewById(R.id.nameEditText);
        phoneEditTextView = (EditText) findViewById(R.id.phoneEditText);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_insert_contact:
                insertContact();

        }

    }


    private Uri insertContact() {

        ContentValues content = new ContentValues();

        content.put(ContactModel.NAME, nameEditTextView.getText().toString());
        content.put(ContactModel.TELEPHONY, phoneEditTextView.getText().toString());

        Uri result = getContentResolver().insert(ContactModel.CONTENT_URI, content);


        if (result != null) {
            Toast.makeText(this, "Contato adicionado com id: " + result.getLastPathSegment(), Toast.LENGTH_SHORT).show();
            finish();
        }

        ContactsHttpManager httpManager = new ContactsHttpManager();
        httpManager.sendContact(nameEditTextView.getText().toString(), phoneEditTextView.getText().toString());

        return result;
    }
}
