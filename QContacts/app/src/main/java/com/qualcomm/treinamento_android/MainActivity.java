package com.qualcomm.treinamento_android;

import android.content.Intent;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.qualcomm.treinamento_android.model.Contact;
import com.qualcomm.treinamento_android.provider.ContactModel;
import com.qualcomm.treinamento_android.service.ContactSyncReceiver;
import com.qualcomm.treinamento_android.service.ContactSyncService;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ContactSyncReceiver.ContactSyncReceiverHandler {

    ListView contactsList;
    TextView emptyContactsListView;
    SwipeRefreshLayout mSwipeRefreshLayout;

    ContactSyncReceiver mReceiver;



    List<Contact> contacts;
    ArrayAdapter<Contact> contactsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        emptyContactsListView = (TextView) findViewById(R.id.no_contacts_view);
        contactsList = (ListView) findViewById(R.id.contacts_list);
        contactsList.setEmptyView(emptyContactsListView);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mReceiver = new ContactSyncReceiver(new Handler());
        mReceiver.setReceiver(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshList();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.activity_main_option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_contacts:
                Intent callAddContacts = new Intent(this, AddContactActivity.class);
                startActivity(callAddContacts);
                break;
        }
        return true;
    }

    @Override
    public void onRefresh() {
        Intent intent = new Intent(this, ContactSyncService.class);
        intent.putExtra("contact_sync_receiver", mReceiver);
        startService(intent);

    }


    @Override
    public void onContactSynced(int resultCode, Bundle resultData) {

        switch (resultCode) {
            case RESULT_OK:
                mSwipeRefreshLayout.setRefreshing(false);
                refreshList();
                break;
        }

    }

    private void refreshList(){
        contacts = new ArrayList<>();

        Cursor contactsCursor = getContentResolver().query(ContactModel.CONTENT_URI, null, null, null, null);
        if (contactsCursor != null) {
            while (contactsCursor.moveToNext()) {
                Contact contact = new Contact(
                        contactsCursor.getString(contactsCursor.getColumnIndex(ContactModel.NAME)),
                        contactsCursor.getString(contactsCursor.getColumnIndex(ContactModel.TELEPHONY)));
                contacts.add(contact);
            }
            contactsCursor.close();
        }
        contactsAdapter = new ArrayAdapter<>(this, R.layout.contacts_list_item, R.id.contact_name, contacts);
        contactsList.setAdapter(contactsAdapter);
    }
}
