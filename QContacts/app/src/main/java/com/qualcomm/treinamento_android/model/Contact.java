package com.qualcomm.treinamento_android.model;


import com.google.gson.annotations.SerializedName;

public class Contact {


    @SerializedName("name")
    private String mName;

    @SerializedName("phone")
    private String mPhone;

    public Contact(String name, String phone) {
        mName = name;
        mPhone = phone;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        this.mPhone = phone;
    }

    @Override
    public String toString() {
        return this.mName + " - " + this.mPhone;
    }


}
