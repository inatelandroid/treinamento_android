package com.qualcomm.treinamento_android.network;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;


public class ContactsHttpManager {


    String contacts_url = "https://contatostreinamento.herokuapp.com/contacts";


    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    public String postContact(String name, String phone) throws IOException {
        OutputStreamWriter wr = null;
        String contentAsString = "";
        try {
            URL url = new URL(contacts_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            conn.setDoInput(true);


            JSONObject data   = new JSONObject();
            JSONObject contact   = new JSONObject();
            contact.put("contact", data);
            data.put("name", name);
            data.put("phone", phone);

            wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(contact.toString());
            wr.flush();

            StringBuilder sb = new StringBuilder();


            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_CREATED || HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                contentAsString = sb.toString();
            } else {
                contentAsString = conn.getResponseMessage();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (wr != null) {wr.close();
            }
        }
        return contentAsString;
    }





    public String getContacts() throws IOException {
        OutputStreamWriter wr = null;
        String contentAsString = "";
        HttpURLConnection conn = null;
        try {

            URL u = new URL(contacts_url);
            conn = (HttpURLConnection) u.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-length", "0");
            conn.setUseCaches(false);
            conn.setAllowUserInteraction(false);
            conn.setConnectTimeout(15000);
            conn.setReadTimeout(10000);
            conn.connect();

            StringBuilder sb = new StringBuilder();


            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_CREATED || HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                contentAsString = sb.toString();
            } else {
                contentAsString = conn.getResponseMessage();
            }

        } finally {
            if (wr != null) {
                wr.close();
            }
        }
        return contentAsString;
    }



    public void sendContact(String name, String phone) {
        SendContactAsyncTask task = new SendContactAsyncTask(name, phone);
        task.execute();
    }


    public void download() {
        DownloadContactAsyncTask task = new DownloadContactAsyncTask();
        task.execute();
    }


    private class SendContactAsyncTask extends AsyncTask<String, Void, String> {

        private String mName;
        private String mPhone;

        SendContactAsyncTask (String name, String phone) {
            mName = name;
            mPhone = phone;
        }

        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return postContact(mName, mPhone);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {


        }
    }



    private class DownloadContactAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                return getContacts();
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        @Override
        protected void onPostExecute(String result) {


        }
    }



}
