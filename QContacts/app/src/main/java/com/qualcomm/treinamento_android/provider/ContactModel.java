package com.qualcomm.treinamento_android.provider;

import android.net.Uri;
import android.provider.BaseColumns;

import com.qualcomm.treinamento_android.QContacts;


public class ContactModel implements BaseColumns {

    public static final String TABLE_NAME = "contacts";

    public static final String NAME = "name";
    public static final String TELEPHONY = "telephony";

    /**
     * The content:// style URL for this table
     */
    public static final Uri CONTENT_URI = Uri.parse("content://" + QContacts.AUTHORITY + "/contact");

    /**
     * The MIME type of {@link #CONTENT_URI} providing a directory of contacts.
     */
    public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.qcontact.contact";

    /**
     * The MIME type of a {@link #CONTENT_URI} sub-directory of a single contact.
     */
    public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.qcontact.contact";

    /**
     * Database creation statement
     */
    public static final String CREATE_CONTACTS_TABLE = "create table "
            + TABLE_NAME + "("
            + _ID	+ " integer primary key autoincrement, "
            + NAME + " text not null,"
            + TELEPHONY + " text not null);";

}
