package com.qualcomm.treinamento_android.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class QContactsDatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "QContacts";

    private Context mContext;

    private static final String DATABASE_NAME = "qcontacts.db";
    private static final int DATABASE_VERSION = 1;

    public QContactsDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(ContactModel.CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            switch (oldVersion) {
                case 1:
            }
        }
    }
}
