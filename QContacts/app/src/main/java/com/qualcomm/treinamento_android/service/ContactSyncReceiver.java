package com.qualcomm.treinamento_android.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class ContactSyncReceiver extends ResultReceiver {


    private ContactSyncReceiverHandler mReceiver;

    public interface ContactSyncReceiverHandler {
        void onContactSynced(int resultCode, Bundle resultData);
    }

    public void setReceiver(ContactSyncReceiverHandler receiver) {
        mReceiver = receiver;
    }


    public ContactSyncReceiver(Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {

        if (mReceiver != null) {
            mReceiver.onContactSynced(resultCode, resultData);
        }
    }

}
