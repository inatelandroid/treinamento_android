package com.qualcomm.treinamento_android.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.gson.Gson;
import com.qualcomm.treinamento_android.model.Contact;
import com.qualcomm.treinamento_android.network.ContactsHttpManager;
import com.qualcomm.treinamento_android.provider.ContactModel;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ContactSyncService extends IntentService {


    public ContactSyncService() {
        super("ContactSyncService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        ContactsHttpManager httpManager = new ContactsHttpManager();

        String result = "";

        try {
            result = httpManager.getContacts();
            List<Contact> contacts = parseContacts(result);
            syncDatabase(contacts);
        } catch (IOException e) {
            e.printStackTrace();
        }

        ResultReceiver rec = intent.getParcelableExtra("contact_sync_receiver");
        Bundle b = new Bundle();
        rec.send(Activity.RESULT_OK, b);

    }

    private List<Contact> parseContacts(String rawContacts) {

        if(rawContacts == null) return null;
        Gson gson = new Gson();
        Contact[] contacts = gson.fromJson(rawContacts, Contact[].class);
        return Arrays.asList(contacts);
    }

    private void syncDatabase(List<Contact> contacts) {

        int deletedRows = getContentResolver().delete(ContactModel.CONTENT_URI, null, null);

        for (Contact contact : contacts) {
            ContentValues values = new ContentValues();
            values.put(ContactModel.NAME, contact.getName());
            values.put(ContactModel.TELEPHONY, contact.getPhone());
            getContentResolver().insert(ContactModel.CONTENT_URI, values);
        }

    }

}
